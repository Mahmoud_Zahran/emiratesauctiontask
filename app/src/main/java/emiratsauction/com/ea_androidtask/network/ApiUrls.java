package emiratsauction.com.ea_androidtask.network;

/**
 * Created by Mahmoud on 07/07/2018.
 */
public class ApiUrls {
//http://operation.tez-tour.com/rest/service/auth?j_username='zzz'&j_password='***'
    public static final String BASE_URL = " http://api.emiratesauction.com/";
    public static final String API_URL_CarList = "v2/carsonline";
    public static final String BASE_URL_IMAGE = "https://cdn.emiratesauction.com/media";
    //public static final String LOGIN_URL = "login";


    /// response type..
   // public static final int SUCCESS_RESPONSE_CODE = 1;
   // public static final int FAIL_RESPONSE_CODE = 0;


    ///  keys ..
    //t_,w_[w],h_[h]/
    public static final String Hight_KEY = "h_";
    public static final String Width_KEY = "w_";
    public static final int Hight_Default_Value = 100;
    public static final int Width_Default_Value = 100;

//    public static final String USERNAME_KEY = "j_username";
//    public static final String PASSWORD_KEY = "j_password";
//    public static final String DATA_KEY = "data";
//    public static final String STATE_KEY = "state";

}
