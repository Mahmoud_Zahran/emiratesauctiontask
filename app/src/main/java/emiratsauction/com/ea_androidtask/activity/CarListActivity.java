package emiratsauction.com.ea_androidtask.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import emiratsauction.com.ea_androidtask.R;
import emiratsauction.com.ea_androidtask.adapter.CarsListAdapter;
import emiratsauction.com.ea_androidtask.model.CarsOnline;
import emiratsauction.com.ea_androidtask.network.ApiInterfaces;
import emiratsauction.com.ea_androidtask.network.ApiMethods;
import emiratsauction.com.ea_androidtask.network.RetrofitSingleton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by Mahmoud on 07/07/2018.
 */
public class CarListActivity extends AppCompatActivity  implements ApiMethods.LoginCallback, SwipeRefreshLayout.OnRefreshListener {
private final static String TAG="CarsListActivity";
private static CarsOnline carsOnlineData=new CarsOnline();
    RecyclerView carsList_recycle_view;
    CarsListAdapter Adapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    static public CarsOnline newUpdateCars;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);
        Log.d(TAG, "onCreate: before running Api");
        carsOnlineData = ApiMethods.CarsOnlineDataLoad(this,this,true);
        //Log.d(TAG, "onCreate: After running Api"+carsOnlineData.getTicks());
        initializeTheList();
        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        carsOnlineData = ApiMethods.CarsOnlineDataLoad(this,this,false);
        //Log.d(TAG, "onCreate: After running Api"+carsOnlineData.alertEn());

    }
    public void initializeTheList(){
try{
        carsList_recycle_view = (RecyclerView) findViewById(R.id.CarsList_recycler_view);
        //    viewPager= (ViewPager) findViewById(R.id.profileViewPager);

        RecyclerView.LayoutManager LayoutManager = new LinearLayoutManager(this);
        carsList_recycle_view.setLayoutManager(LayoutManager);
        if (carsOnlineData.getCars()!=null) {

            Adapter = new CarsListAdapter(this, carsOnlineData.getCars());

            final Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);

                    // Fetching data from server
                    loadRecyclerViewData();


                    handler.postDelayed(this, Long.parseLong(carsOnlineData.getTicks()) * 1000);
                    Log.d(TAG, "run: Ticks in seconds:" + Long.parseLong(carsOnlineData.getTicks()) * 1000);
                }
            };

            handler.post(runnable);
            carsList_recycle_view.setAdapter(Adapter);
        }
}catch (Exception e){
    Toast.makeText(this,"Loading Error please try again",Toast.LENGTH_LONG).show();
    Log.d(TAG, "initializeTheList: "+e.getMessage());
}
    }

    @Override
    public void loginData(boolean state) {

    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {

        // Fetching data from server
        loadRecyclerViewData();
    }
    private void loadRecyclerViewData() {
        // Showing refresh animation before making http call
        mSwipeRefreshLayout.setRefreshing(true);

       // newUpdateCars = ApiMethods.CarsOnlineDataLoad(getApplicationContext(),CarListActivity.this,false);


            Retrofit retrofit = RetrofitSingleton.getInstance();
            ApiInterfaces.CarListApi service = retrofit.create(ApiInterfaces.CarListApi.class);

            if (/*NetworkingUtils.isNetworkConnected()*/true) {
                Call<CarsOnline> call = service.getApiData();
                call.enqueue(new Callback<CarsOnline>() {
                    @Override
                    public void onResponse(Call<CarsOnline> call, Response<CarsOnline> response) {

                        newUpdateCars= response.body();


                        if (newUpdateCars.getCars() != null && newUpdateCars.getCars().size() > 0) {
                            if (Adapter != null && Adapter.getItemCount() > 0) {
                                Adapter.resetData(newUpdateCars.getCars());
                                mSwipeRefreshLayout.setRefreshing(false);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CarsOnline> call, Throwable t) {
                        Log.d("onFailure", t.toString());
                        // Stopping swipe refresh
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            }









    }
}
