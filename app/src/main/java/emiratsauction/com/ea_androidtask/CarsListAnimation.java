package emiratsauction.com.ea_androidtask;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Path;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;


public class CarsListAnimation {

    public static Animation ShakeAnimation;
    public static ObjectAnimator MoveobjectAnimator;
    public static ValueAnimator ExpandValueAnimator;
    public static AnimatorSet StretchAnimatorSet;

    //**********************************************************//
    public static void Shake(View TheView , Context context) {

        ShakeAnimation = AnimationUtils.loadAnimation(context, R.anim.bounce);
        double animationDuration = 0.80 * 1000;

        ShakeAnimation.setDuration((long) animationDuration);
        ShakeAnimation.setInterpolator(interpolator);

        TheView.startAnimation(ShakeAnimation);
    }
    static Interpolator interpolator = new Interpolator() {
        @Override
        public float getInterpolation(float time) {
            return (float) (-1 * Math.pow(Math.E, -time / 0.17) * Math.cos(40 * time) + 1);}
    };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void Move(View TheView , int X , int Y , int Time) {

        float x = TheView.getX();
        float y = TheView.getY();
        Path path = new Path();

        path.moveTo(x + 0, y + 0);
        path.lineTo(x + X, y + Y);

        MoveobjectAnimator = ObjectAnimator.ofFloat(TheView, View.X, View.Y, path);
        MoveobjectAnimator.setDuration(Time);
        MoveobjectAnimator.start();
    }
    public static void ExpandAndCollapse(final View TheView, int Duration, int TargetSize, final String WidthOrHeight) {

        int Previous = 200;

        if (WidthOrHeight == "W")
            Previous = TheView.getWidth();

        else if (WidthOrHeight == "H")
            Previous = TheView.getHeight();


        ExpandValueAnimator = ValueAnimator.ofInt(Previous, TargetSize);
        ExpandValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                if (WidthOrHeight == "W")
                    TheView.getLayoutParams().width = (int) animation.getAnimatedValue();

                else if (WidthOrHeight == "H")
                    TheView.getLayoutParams().height = (int) animation.getAnimatedValue();

                TheView.requestLayout();
            }


        });

        ExpandValueAnimator.setDuration(Duration);
        ExpandValueAnimator.start();
    }
    public static void scaleView(View TheView, float StartScale, float EndScale) {
        Animation anim = new ScaleAnimation(
                1f, 1f, // Start and end values for the X axis scaling
                StartScale, EndScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(500);
        TheView.startAnimation(anim);
    }
    public static void Stretch(View TheView , String WidthOrHeight){
        StretchAnimatorSet = new AnimatorSet();
        StretchAnimatorSet.playTogether(
                ObjectAnimator.ofFloat(TheView, "scale"+WidthOrHeight, 1.0f, 0f).setDuration(700)
        );
        StretchAnimatorSet.start();
    }
}
