package emiratsauction.com.ea_androidtask.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Mahmoud on 07/07/2018.
 */

public class AuctionInfo implements Serializable {

    @SerializedName("bids")
    int bids;
    @SerializedName("endDate")
    int endDate;
    @SerializedName("endDateEn")
    String endDateEn;
    @SerializedName("endDateAr")
    String endDateAr;
    @SerializedName("currencyEn")
    String currencyEn;
    @SerializedName("currencyAr")
    String currencyAr;
    @SerializedName("currentPrice")
    int currentPrice;
    @SerializedName("minIncrement")
    int minIncrement;
    @SerializedName("lot")
    int lot;
    @SerializedName("priority")
    int priority;
    @SerializedName("VATPercent")
    int VATPercent;
    @SerializedName("isModified")
    int isModified;
    @SerializedName("itemid")
    int itemid;
    @SerializedName("iCarId")
    int iCarId;
    @SerializedName("iVinNumber")
    String iVinNumber;

    public int getBids() {
        return bids;
    }

    public void setBids(int bids) {
        this.bids = bids;
    }

    public int getEndDate() {
        return endDate;
    }

    public void setEndDate(int endDate) {
        this.endDate = endDate;
    }

    public String getEndDateEn() {
        return endDateEn;
    }

    public void setEndDateEn(String endDateEn) {
        this.endDateEn = endDateEn;
    }

    public String getEndDateAr() {
        return endDateAr;
    }

    public void setEndDateAr(String endDateAr) {
        this.endDateAr = endDateAr;
    }

    public String getCurrencyEn() {
        return currencyEn;
    }

    public void setCurrencyEn(String currencyEn) {
        this.currencyEn = currencyEn;
    }

    public String getCurrencyAr() {
        return currencyAr;
    }

    public void setCurrencyAr(String currencyAr) {
        this.currencyAr = currencyAr;
    }

    public int getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(int currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getMinIncrement() {
        return minIncrement;
    }

    public void setMinIncrement(int minIncrement) {
        this.minIncrement = minIncrement;
    }

    public int getLot() {
        return lot;
    }

    public void setLot(int lot) {
        this.lot = lot;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getVATPercent() {
        return VATPercent;
    }

    public void setVATPercent(int VATPercent) {
        this.VATPercent = VATPercent;
    }

    public int getIsModified() {
        return isModified;
    }

    public void setIsModified(int isModified) {
        this.isModified = isModified;
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    public int getiCarId() {
        return iCarId;
    }

    public void setiCarId(int iCarId) {
        this.iCarId = iCarId;
    }

    public String getiVinNumber() {
        return iVinNumber;
    }

    public void setiVinNumber(String iVinNumber) {
        this.iVinNumber = iVinNumber;
    }
}
